import React from 'react';

export const FinalDashboard = ({
  final,
  setAnswer,
}) => (
  <div className="dashboard__final-container">
    <div className="row row--center">
      <div className="column">
        <strong>Gracz 1</strong>
        {final.questions && final.questions.map((question, questionNumber) => (
          <div
            className="dashboard__final-question"
            key={`final-${questionNumber}`}
          >
            {questionNumber+1}. {question.text}
            <div className="dashboard__final-answers">
              {question.possibleAnswers && question.possibleAnswers.map((answer, answerNumber) => (
                <div
                  key={answerNumber}
                  className={`dashboard__answer${answerNumber === question.possibleAnswers.indexOf(question.givenAnswerPlayer1) ? ' dashboard__answer--checked' : ''}`}
                >
                  <label className="dashboard__answer-label">
                    <input
                      type="radio"
                      checked={answerNumber === question.possibleAnswers.indexOf(question.givenAnswerPlayer1)}
                      onChange={() => setAnswer(questionNumber, 1, answerNumber)}
                    />
                    <div>{answer.text}</div> 
                    <div>{answer.points}</div>
                  </label>
                </div>
              ))}
              <div>
                <input type="text" list={`${questionNumber}-answers`} onChange={(event) => setAnswer(questionNumber, 1, event.currentTarget.value, {custom: true})}/>
                <datalist id={`${questionNumber}-answers`}>
                  {question.possibleAnswers && question.possibleAnswers.map((answer) => (
                    <option value={answer.text} />
                  ))}
                </datalist>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="column">
        <strong>Gracz 2</strong>
        {final.questions && final.questions.map((question, questionNumber) => (
          <div
            className="dashboard__final-question"
            key={`final-${questionNumber}`}
          >
            {questionNumber+1}. {question.text}
            <div className="dashboard__final-answers">
              {question.possibleAnswers && question.possibleAnswers.map((answer, answerNumber) => (
                <div
                  key={answerNumber}
                  className={`dashboard__answer${answerNumber === question.possibleAnswers.indexOf(question.givenAnswerPlayer2) ? ' dashboard__answer--checked' : ''}`}
                >
                  <label className="dashboard__answer-label">
                    <input
                      key={answerNumber}
                      type="radio"
                      checked={answerNumber === question.possibleAnswers.indexOf(question.givenAnswerPlayer2)}
                      onChange={() => setAnswer(questionNumber, 2, answerNumber)}
                    />
                    <div>{answer.text}</div> 
                    <div>{answer.points}</div>
                  </label>
                </div>
              ))}
              <div>
              <input type="text" list={`${questionNumber}-answers`} onChange={(event) => setAnswer(questionNumber, 2, event.currentTarget.value, {custom: true})}/>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  </div>
);
