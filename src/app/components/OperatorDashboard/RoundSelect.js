import React from 'react';

export const RoundSelect = ({
  handleQuestionsSelectChange,
  previousRound,
  nextRound,
  game,
  rounds,
}) => (
  <div className="row row--center">
    <button className="button" onClick={previousRound}>&lt; poprzednie</button>
    <select
      className="question-select"
      onChange={handleQuestionsSelectChange}
      value={game.isFinal ? 'final' : rounds.currentRoundNumber}
    >
      {rounds.rounds.map((round, index) => (
        <option key={index} value={index}>{`${index+1}. ${round.question}`}</option>
        ))}
        <option value="final">FINAŁ</option>
    </select>
    <button className="button" onClick={nextRound}>następne &gt;</button>
  </div>
);
