import React from 'react';

export const RoundDashboard = ({
  currentRound,
  currentRoundNumber,
  selectAnswer,
  teamAnswering,
  setTeamPoints,
  totalPointsTeamLeft,
  totalPointsTeamRight,
  changeTeamAnswering,
  addMistake,
  removeMistake,
}) => (
<div className="round-wrapper">
  <div
    className={`team-stats__wrapper ${teamAnswering === 'left' ? 'team-stats__wrapper--selected' : ''}`}
    onClick={() => changeTeamAnswering('left')}
  >
    <h3
      className={`team-stats__heading ${teamAnswering === 'left' ? 'team-stats__heading--selected' : ''}`}
    >
      Drużyna LEWA
    </h3>
    <div>
      błędy: {currentRound.mistakesTeamLeft}
    </div>
    <div>
      punkty runda: {currentRound.pointsTeamLeft}
    </div>
    <div>
      <br />
      punkty ogółem: {totalPointsTeamLeft}
    </div>
  </div>
  <div className="dashboard__round-container">
    <div className="row row--center">
      <div className="dashboard__answers">
        {currentRound.answers.map((answer, index) => (
          <div
            key={index}
            className={`dashboard__answer${answer.isSelected ? ' dashboard__answer--checked' : ''}`}
          >
            <label className="dashboard__answer-label">
              <input
                type="checkbox"
                checked={answer.isSelected}
                onChange={() => selectAnswer(currentRoundNumber, index, answer.points, teamAnswering, !answer.isSelected)}
              />
              <div>{answer.text}</div> 
              <div>{answer.points}</div>
            </label>
          </div>
        ))}
      </div>
    </div>
    {teamAnswering ? (
      <div className="row row--center">
        <button
          className="button button--danger"
          onClick={() => addMistake(currentRoundNumber, teamAnswering, teamAnswering === 'left' ? currentRound.mistakesTeamLeft : currentRound.mistakesTeamRight)}
        >
            + POMYŁKA</button>
        <button
          className="button"
          onClick={() => removeMistake(currentRoundNumber, teamAnswering, teamAnswering === 'left' ? currentRound.mistakesTeamLeft : currentRound.mistakesTeamRight)}
        >
          - pomyłka
        </button>
      </div>
    ) : (
      <div className="row row--center">
        <button
          className="button"
          onClick={() => removeMistake(currentRoundNumber, 'left', currentRound.mistakesTeamLeft)}
        >
          - pomyłka LEWA
        </button>
        <button
          className="button button--danger"
          onClick={() => addMistake(currentRoundNumber, 'left', currentRound.mistakesTeamLeft)}
        >
            + POMYŁKA LEWA</button>
        <button
          className="button button--danger"
          onClick={() => addMistake(currentRoundNumber, 'right', currentRound.mistakesTeamRight)}
        >
            + POMYŁKA PRAWA</button>
        <button
          className="button"
          onClick={() => removeMistake(currentRoundNumber, 'right', currentRound.mistakesTeamRight)}
        >
          - pomyłka PRAWA
        </button>
      </div>
    )}
    <div className="row row--center">
      punkty runda: {currentRound.pointsTotal}
    </div>
    <div className="row row--center">
      <button
        className="button button--danger"
        onClick={() => setTeamPoints(teamAnswering, currentRoundNumber, currentRound.pointsTotal)}
      >
        + PRZEKAŻ DRUŻYNIE PUNKTY
      </button>
      <button
        className="button"
        onClick={() => setTeamPoints(teamAnswering, currentRoundNumber, 0)}
      >
        - odbierz drużynie punkty
      </button>
    </div>
  </div>
  <div
    className={`team-stats__wrapper team-stats__wrapper--right ${teamAnswering === 'right' ? 'team-stats__wrapper--selected-right' : ''}`}
    onClick={() => changeTeamAnswering('right')}
    >
    <h3
      className={`team-stats__heading ${teamAnswering === 'right' ? 'team-stats__heading--selected' : ''}`}
    >
      Drużyna PRAWA
    </h3>
    <div>
      błędy: {currentRound.mistakesTeamRight}
    </div>
    <div>
      punkty runda: {currentRound.pointsTeamRight}
    </div>
    <div>
      <br />
      punkty ogółem: {totalPointsTeamRight}
    </div>
  </div>
</div>
);
