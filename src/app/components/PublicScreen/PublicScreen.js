import React from 'react';

import Round from './Round';

import './PublicScreen.scss';

const PublicScreen = ({
  game,
  rounds,
}) => (
  <div className="public-screen__container">
    {game.isIntro && (
      <div className="intro__container">
        <h1 className="intro__title">
          {game.name}
        </h1>
      </div>
    )}
    {game.isRound && (
      <Round rounds={rounds}/>
    )}
  {game.isFinal && (
    <div className="final__container">
      Final
    </div>
  )}
  {game.isOutro && (
      <div className="outro__container">
        Outro
      </div>
  )}
  </div>
)

export default PublicScreen;
