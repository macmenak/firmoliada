import React from 'react';

const Round = ({
  rounds
}) => (
  <div className="round__container">
    <div className="round__answers-mistakes-wrapper">
      <div className="round__mistakes round__mistakes--left">
        {new Array(rounds.rounds[rounds.currentRoundNumber].mistakesTeamLeft).fill(1).map(() => (
          <div className="mistake"></div>
        ))}
      </div>
      <div className="round__answers-wrapper">
        <div className="round__answers">
          {rounds && rounds.rounds && rounds.rounds && rounds.rounds[rounds.currentRoundNumber].answers.map((answer, index) => {
              return (
                <div
                  className="round__answer"
                >
                  <p className="round__answer-index">{index+1}</p> 
                  <p className="round__answer-text">{answer.isSelected ? answer.text : '... ... ... ... ... ... ... ... ... ...'}</p>
                  <p className="round__answer-points">{answer.isSelected ? answer.points : '--'}</p>
                </div>
              )
            }
          )}
          <div className="round__points-total">
            suma {rounds.rounds[rounds.currentRoundNumber].pointsTotal}
          </div>
        </div>
      </div>
      <div className="round__mistakes round__mistakes--right">
        {new Array(rounds.rounds[rounds.currentRoundNumber].mistakesTeamRight).fill(1).map(() => (
          <div className="mistake"></div>
        ))}
      </div>
    </div>
    <div className="round__points round__points--left">
      {rounds.totalPointsTeamLeft}
    </div>
    <div className="round__points round__points--right">
      {rounds.totalPointsTeamRight}
    </div>
  </div>
);

export default Round;