import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import {
  setGameMode,
  setGameName,
} from '../../store/actions/game';
import {
  setRound,
  initializeRounds,
  selectAnswer,
  setMistakes,
  setTeamPoints
} from '../../store/actions/rounds';
import {
  initializeFinal,
  setAnswer
} from '../../store/actions/final';

import PublicScreen from '../../components/PublicScreen/PublicScreen';
import { RoundSelect } from '../../components/OperatorDashboard/RoundSelect';
import { FinalDashboard } from '../../components/OperatorDashboard/FinalDashboard';

import './OperatorDashboard.scss';

import GAME_CONFIG from '../../game_config';
import { RoundDashboard } from '../../components/OperatorDashboard/RoundDashboard';

const copyStyles = (sourceDoc, targetDoc) => {
  Array.from(sourceDoc.styleSheets).forEach(styleSheet => {
    if (styleSheet.cssRules) { // for <style> elements
      const newStyleEl = sourceDoc.createElement('style');

      Array.from(styleSheet.cssRules).forEach(cssRule => {
        newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText));
      });

      targetDoc.head.appendChild(newStyleEl);
    } else if (styleSheet.href) { // for <link> elements loading CSS from a URL
      const newLinkEl = sourceDoc.createElement('link');

      newLinkEl.rel = 'stylesheet';
      newLinkEl.href = styleSheet.href;
      targetDoc.head.appendChild(newLinkEl);
    }
  });
}

class MyWindowPortal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.containerEl = document.createElement('div');
    this.externalWindow = null;
  }

  componentDidMount() {
    this.externalWindow = window.open('', '', 'width=600,height=400,left=200,top=200');
    copyStyles(document, this.externalWindow.document);
    this.externalWindow.document.body.appendChild(this.containerEl);
    this.externalWindow.addEventListener('beforeunload', () => {
      this.props.closeWindowPortal();
    });
  }

  componentWillUnmount() {
    this.externalWindow.close();
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.containerEl);
  }
}

class OperatorDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showWindowPortal: false,
      teamAnswering: '',
    };

    this.openWindowPortal = this.openWindowPortal.bind(this);
    this.closeWindowPortal = this.closeWindowPortal.bind(this);
    this.handleQuestionsSelectChange = this.handleQuestionsSelectChange.bind(this);
    this.changeTeamAnswering = this.changeTeamAnswering.bind(this);
    this.nextRound = this.nextRound.bind(this);
    this.previousRound = this.previousRound.bind(this);
    this.addMistake = this.addMistake.bind(this);
    this.removeMistake = this.removeMistake.bind(this);
    
  }
  
  componentDidMount() {
    const {
      setGameName,
      setGameMode,
      setRound,
      initializeRounds,
      initializeFinal
    } = this.props;
    setGameName(GAME_CONFIG.name);
    initializeRounds(GAME_CONFIG.questions, GAME_CONFIG.answers);
    initializeFinal(GAME_CONFIG.questions, GAME_CONFIG.answers);
    setGameMode('round');
    setRound(0);
  }

  openWindowPortal() {
    this.setState({
      showWindowPortal: true
    });
  }

  closeWindowPortal() {
    this.setState({
      showWindowPortal: false
    });
  }

  handleQuestionsSelectChange(event) {
    const {
      setRound,
      setGameMode
    } = this.props;
    const selectedValue = event.currentTarget.value;
    if (selectedValue === 'final') {
      setGameMode('final');
    } else {
      setGameMode('round');
      setRound(Number.parseInt(selectedValue, 10));
      this.setState({
        teamAnswering: ''
      });
    }
  }

  changeTeamAnswering(teamAnswering) {
    this.setState({
      teamAnswering
    });
  }

  checkIfAllAnswersSelected(answers) {
    return answers.every(answer => answer.isSelected);
  }

  nextRound() {
    const {
      setRound,
      setGameMode,
      rounds,
      game,
    } = this.props;
    const { currentRoundNumber } = rounds;
    const currentRound = rounds.rounds && rounds.rounds[currentRoundNumber];
    let alertMessage = '';

    if(!this.checkIfAllAnswersSelected(currentRound.answers)) {
      alertMessage = 'Nie sprawdzono wszystkich odpowiedzi';
    }

    if(currentRound.pointsTotal && !currentRound.pointsTeamLeft && !currentRound.pointsTeamRight) {
      alertMessage = `${alertMessage}\nNie przekazano punktów żadnej drużynie`;
    }

    if (alertMessage) {
      return alert(alertMessage);
    }

    if (!game.isFinal && currentRoundNumber < rounds.rounds.length - 1) {
      setRound(currentRoundNumber + 1);
      this.setState({
        teamAnswering: ''
      });
    }
    if (currentRoundNumber === rounds.rounds.length - 1) {
      setGameMode('final');
    }
    
  }

  previousRound() {
    const {
      game,
      setRound,
      setGameMode,
      rounds,
    } = this.props;
    const { currentRoundNumber } = rounds;
    if (!game.isFinal && currentRoundNumber > 0) {
      setRound(currentRoundNumber - 1);
      this.setState({
        teamAnswering: ''
      });
    } else if (game.isFinal) {
      setRound(rounds.rounds.length - 1);
      setGameMode('round');
    }
  }

  addMistake(roundNumber, teamAnswering, currentMistakesCount) {
    if (teamAnswering && currentMistakesCount < 3) {
      this.props.setMistakes(roundNumber, teamAnswering, currentMistakesCount + 1)
    }
  }

  removeMistake(roundNumber, teamAnswering, currentMistakesCount) {
    if (teamAnswering && currentMistakesCount > 0) {
      this.props.setMistakes(roundNumber, teamAnswering, currentMistakesCount - 1)
    }
  }

  render() {
    const {
      game,
      rounds,
      final,
      selectAnswer,
      setTeamPoints,
      setAnswer
    } = this.props;
    const currentRound = rounds.rounds[rounds.currentRoundNumber];
    const {
      teamAnswering
    } = this.state;
    return (
      <div>
        <div className="operator-dashboard">
          <div className="row">
            {!this.state.showWindowPortal ? (
              <button className="button" onClick={this.openWindowPortal}>
                Open window
              </button>
            ) : (
              <button className="button button--active">
                WINDOW ACTIVE
              </button>
            )}
          </div>
          <RoundSelect
            handleQuestionsSelectChange={this.handleQuestionsSelectChange}
            previousRound={this.previousRound}
            nextRound={this.nextRound}
            game={game}
            rounds={rounds}
          />
          {game.isRound && (
            <RoundDashboard
              currentRound={currentRound}
              currentRoundNumber={rounds.currentRoundNumber}
              selectAnswer={selectAnswer}
              teamAnswering={teamAnswering}
              setTeamPoints={setTeamPoints}
              totalPointsTeamLeft={rounds.totalPointsTeamLeft}
              totalPointsTeamRight={rounds.totalPointsTeamRight}
              changeTeamAnswering={this.changeTeamAnswering}
              addMistake={this.addMistake}
              removeMistake={this.removeMistake}
            />
          )}
          {game.isFinal && (
            <FinalDashboard
              final={final}
              setAnswer={setAnswer}
            />
          )}
        </div>
        {this.state.showWindowPortal && (
          <MyWindowPortal closeWindowPortal={this.closeWindowPortal}>
            <PublicScreen 
              game={game}
              rounds={rounds}
            />
          </MyWindowPortal>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  game: state.game,
  questions: state.questions,
  rounds: state.rounds,
  final: state.final
});

const mapDispatchToProps = ({
  setGameMode,
  setGameName,
  initializeRounds,
  setRound,
  selectAnswer,
  setMistakes,
  setTeamPoints,
  initializeFinal,
  setAnswer
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OperatorDashboard);
