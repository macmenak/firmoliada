import React from 'react';

import './App.css';
import OperatorDashboard from './containers/OperatorDashboard/OperatorDashboard';


const App = () => (
  <OperatorDashboard />
)

export default App;
