import questions from './questions.json';
import answers from './answers.json';

export default  {
  name: 'Lidliada',
  questions,
  answers,
  finalCounterPlayer1: 45,
  finalCounterPlayer2: 60,
}