const prefix = '[rounds]';

export const SET_ROUND = `${prefix} SET_ROUND`;
export const INITIALIZE_ROUNDS = `${prefix} INITIALIZE_ROUNDS`;
export const SELECT_ANSWER = `${prefix} SELECT_ANSWER`;
export const SET_MISTAKES = `${prefix} SET_MISTAKES`;
export const SET_TEAM_POINTS = `${prefix} SET_TEAM_POINTS`;

export const setRound = roundNumber => ({
  type: SET_ROUND,
  roundNumber
});

export const initializeRounds = (questions, answers) => ({
  type: INITIALIZE_ROUNDS,
  questions,
  answers
});

export const selectAnswer = (roundNumber, answerNumber, points, team, isSelected) => ({
  type: SELECT_ANSWER,
  roundNumber,
  answerNumber,
  points,
  team,
  isSelected
});

export const setMistakes = (roundNumber, team, mistakesCount) => ({
  type: SET_MISTAKES,
  roundNumber,
  team,
  mistakesCount
});

export const setTeamPoints = (team, roundNumber, roundPoints) => ({
  type: SET_TEAM_POINTS,
  roundNumber,
  roundPoints,
  team
});