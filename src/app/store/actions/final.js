const prefix = '[final]';

export const INITIALIZE_FINAL = `${prefix} INITIALIZE_FINAL`;
export const SET_ANSWER = `${prefix} SET_ANSWER`;

export const initializeFinal = (questions, answers) => ({
  type: INITIALIZE_FINAL,
  questions,
  answers
});

export const setAnswer = (questionNumber, player, answer, { custom } = { custom: false }) => ({
  type: SET_ANSWER,
  questionNumber,
  player,
  answer,
  custom,
});