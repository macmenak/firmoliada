const prefix = '[game]';

export const SET_GAME_MODE = `${prefix} SET_GAME_MODE`;
export const SET_GAME_NAME = `${prefix} SET_GAME_NAME`;

export const setGameMode = mode => ({
  type: SET_GAME_MODE,
  mode
});

export const setGameName = name => ({
  type: SET_GAME_NAME,
  name
});
