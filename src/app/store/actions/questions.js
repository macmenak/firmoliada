const prefix = '[questions]';

export const INITIALIZE_QUESTIONS_AND_ANSWERS = `${prefix} INITIALIZE_QUESTIONS_AND_ANSWERS`;

export const initializeQuestionsAndAnswers = (questions, answers) => ({
  type: INITIALIZE_QUESTIONS_AND_ANSWERS,
  questions,
  answers
});