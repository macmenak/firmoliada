import {
  SET_GAME_MODE,
  SET_GAME_NAME
} from '../actions/game';

const initialState = {
  name: 'firmoliada',
  isSecondWindow: false,
  isSound: false,
  soundPlaying: '',
  isIntro: false,
  isRound: false,
  isFinal: false,
  isOutro: false,
  pointsTeamLeft: 0,
  pointsTeamRight: 0,
  winningTeam: 0,
}

const game = (state = initialState, action) => {
  switch (action.type) {
    case SET_GAME_NAME:
      return {
        ...state,
        name: action.name
      }
    case SET_GAME_MODE:
      const { mode } = action;
      return {
        ...state,
        isIntro: mode === 'intro',
        isRound: mode === 'round',
        isFinal: mode === 'final',
        isOutro: mode === 'outro',
      }
    default:
      return state;
  }
}

export default game;