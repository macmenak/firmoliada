import {
  SET_ROUND,
  INITIALIZE_ROUNDS,
  SELECT_ANSWER,
  SET_MISTAKES,
  SET_TEAM_POINTS
} from '../actions/rounds';

const initialState = {
  currentRoundNumber: 0,
  totalPointsTeamLeft : 0,
  totalPointsTeamRight : 0,
  rounds: [
    {
      pointsTotal : 0,
      mistakesTeamLeft : 0,
      mistakesTeamRight : 0,
      answers: []
    }
  ]
}

const rounds = (state = initialState, action) => {
  switch (action.type) {
    case SET_ROUND:
      return {
        ...state,
        currentRoundNumber: (action.roundNumber >= 0 && action.roundNumber < state.rounds.length) ? action.roundNumber : 0
      }
    case SELECT_ANSWER:
      const addOrSubstract = action.isSelected ? 1 : -1;
      return {
        ...state,
        rounds: state.rounds.map((round, index) => {
          return index === action.roundNumber ? (
            {
              ...round,
              pointsTotal: round.pointsTotal + addOrSubstract * Number.parseInt(action.points, 10),
              answers: round.answers.map((answer, index) => {
                return index === action.answerNumber ? (
                  {
                    ...answer,
                    isSelected: action.isSelected,
                  }
                ) : answer
              })
            }
          ) : round
        })
      }
      case SET_TEAM_POINTS:
        return {
          ...state,
          totalPointsTeamLeft: action.team === 'left' ? state.rounds.filter((round, index) => index !== action.roundNumber && round).reduce((previousValue, currentRound) => previousValue + currentRound.pointsTeamLeft, action.roundPoints) : state.totalPointsTeamLeft,
          totalPointsTeamRight: action.team === 'right' ? state.rounds.filter((round, index) => index !== action.roundNumber && round).reduce((previousValue, currentRound) => previousValue + currentRound.pointsTeamRight, action.roundPoints) : state.totalPointsTeamRight,
          rounds: state.rounds.map((round, index) => {
            return index === action.roundNumber ? (
              {
                ...round,
                pointsTeamLeft: action.team === 'left' ? action.roundPoints : 0,
                pointsTeamRight: action.team === 'right' ? action.roundPoints : 0
              }
            ) : round
          })
        }
    case SET_MISTAKES:
      return {
        ...state,
        rounds: state.rounds.map((round, index) => {
          return index === action.roundNumber ? (
            {
              ...round,
              mistakesTeamLeft: action.team === 'left' ? action.mistakesCount :round.mistakesTeamLeft,
              mistakesTeamRight: action.team === 'right' ? action.mistakesCount :round.mistakesTeamRight
            }
          ) : round
        })
      }
    case INITIALIZE_ROUNDS:
      return {
        currentRoundNumber: 0,
        totalPointsTeamLeft : 0,
        totalPointsTeamRight : 0,
        pointsTeamRight : 0,
        rounds: action.questions.filter(question => question.is_final && !isNaN(question.is_final) && !Number.parseInt(question.is_final,10)).map(question => {
          return ({
            question: question.text,
            answers: action.answers.filter(answer => answer.question_id === question.ID).map(answer => {
              return ({
                text: answer.text,
                points: answer.points,
                isSelected: false,
              })
            }),
            pointsTotal : 0,
            pointsTeamLeft: 0,
            pointsTeamRight: 0,
            mistakesTeamLeft : 0,
            mistakesTeamRight : 0,
          })
        })
      }
    default:
      return state;
  }
}

export default rounds;