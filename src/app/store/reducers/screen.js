import {
  SYNC_GAME,
  SYNC_ROUNDS,
} from '../actions/screen';

const initialState = {
  rounds: [
    {
      pointsTotal : 0,
      mistakesTeamLeft : 0,
      mistakesTeamRight : 0,
      answers: []
    }
  ]
}

const screen = (state = initialState, action) => {
  switch (action.type) {
    case SYNC_ROUNDS:
      return {
        ...state,
        rounds: action.payload,
      }
    case SYNC_GAME:
      return {
        ...state,
        rounds: action.payload,
      }
    default:
      return state;
  }
}