import {
  INITIALIZE_QUESTIONS_AND_ANSWERS
} from '../actions/questions';

const initialState = {
  questions: [
    {
      text: '',
      isFinal: false,
      answers: [
        {
          text: '',
          points: 0,
          isSelected: false,
        }
      ],
    }
  ]
}

const questions = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE_QUESTIONS_AND_ANSWERS:
      return {
        questions: action.questions.map(question => {
          return ({
            text: question.text,
            isFinal: question.is_final,
            answers: action.answers.filter(answer => answer.question_id === question.ID).map(answer => {
              return ({
                text: answer.text,
                points: answer.points,
                isSelected: false,
              })
            })
          })
        })
      }
    default:
      return state;
  }
}

export default questions;