import { combineReducers } from 'redux';

import questions from './questions';
import rounds from './rounds';
import final from './final';
import game from './game';

const reducers = combineReducers({
  questions,
  rounds,
  final,
  game
})

export default reducers;