import {
  INITIALIZE_FINAL,
  SET_ANSWER
} from '../actions/final';

const initialState = {
  pointsTotal : 0,
  currentPlayer: 0,
  isPlayer1AnswersHidden: false,
  counter: 0,
  currentAnswerVisiblePlayer1: 0,
  currentAnswerVisiblePlayer2: 0,
  questions: [
    {
      text: '',
      possibleAnswers: [
        {
          text: '',
          points: 0,
        }  
      ],
      givenAnswerPlayer1: 0 || '',
      givenAnswerPlayer2: 0 || '',
    },
  ],
};

const final = (state = initialState, action) => {
  switch (action.type) {
    case SET_ANSWER:

    const possibleAnswerFound = !!action.custom && state.questions[action.questionNumber].possibleAnswers.find(
      possibleAnswer => possibleAnswer.text.toLowerCase() === action.answer.toLowerCase()
    );
    // const possibleAnswerPoints = possibleAnswerFound;
    const actionAnswer = possibleAnswerFound || state.questions[action.questionNumber].possibleAnswers[action.answer];
      return {
        ...state,
        //TODO: pointsTotal: possibleAnswerFound ? state.questions.filter((question, index) => index !== action.roundNumber && round).reduce((previousValue, currentRound) => previousValue + currentRound.pointsTeamLeft, action.roundPoints) : state.pointsTotal,
        questions: state.questions.map((question, index) => {
          return index === action.questionNumber ? (
            {
              ...question,
              givenAnswerPlayer1: action.player === 1 ? actionAnswer : question.givenAnswerPlayer1,
              givenAnswerPlayer2: action.player === 2 ? actionAnswer : question.givenAnswerPlayer2
            }
          ) : question
        })
      }
    case INITIALIZE_FINAL:
      return {
        pointsTotal: 0,
        currentPlayer : 1,
        isPlayer1AnswersHidden: false,
        counter: 0,
        currentAnswerVisiblePlayer1: 0,
        currentAnswerVisiblePlayer2: 0,
        questions: action.questions.filter(question => question.is_final && !isNaN(question.is_final) && Number.parseInt(question.is_final,10)).map(question => {
          return ({
            text: question.text,
            possibleAnswers: action.answers.filter(answer => answer.question_id === question.ID).map(answer => {
              return ({
                text: answer.text,
                points: answer.points,
              })
            }),
            givenAnswerPlayer1: 0 || '',
            givenAnswerPlayer2: 0 || '',
          })
        })
      }
    default:
      return state;
  }
}

export default final;